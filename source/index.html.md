---
title: Hivedex.io API Docs

# language_tabs: # must be one of https://git.io/vQNgJ
#   - shell
#   - ruby
#   - python
#   - javascript

toc_footers:
  - <a href='https://github.com/slatedocs/slate'>Documentation Powered by Slate</a>

includes:
  - errors

search: true

code_clipboard: true

meta:
  - name: description
    content: API documentation for Hivedex.io 
---

# Introduction

All the APIs listed on this documentation are open for the public use. There is rate limiting applied on the API server but a general user shouldn't hit the limit.

The base URL is `https://api.hivedex.io`

<!-- # Authentication

No authentication is necessary. -->

# Public APIs

## Get Pairs

> Example:

```shell
curl "https://api.hivedex.io/api/pairs"
```

> Response:

```json
[
  {
    "ticker_id":"HIVE_HBD",
    "base":"HIVE",
    "target":"HBD"
  }
]
```

`GET /api/pairs`

Get all the pairs available on hivedex.io


## Get Summary

> Example:

```shell
curl "https://api.hivedex.io/api/summary"
```

> Response:

```json
[
  {
    "trading_pairs": "HIVE_HBD",
    "last_price": "0.383381",
    "lowest_ask": "0.382914",
    "highest_bid": "0.381838",
    "base_volume": "204917.236",
    "quote_volume": "77446.004",
    "price_change_percent_24h": 0.057465652729163486,
    "highest_price_24h": "0.385845",
    "lowest_price_24h": "0.360000"
  }
]
```

`GET /api/summary`

Get summary of all the pairs.


## Get orderbook

Get the orderbook for a pair.

> Example:

```shell
curl "https://api.hivedex.io/api/orderbook/HIVE_HBD?depth=2"
```

> Response:

```json
{
  "ticker_id":"HIVE_HBD",
  "timestamp":1674319548000,
  "bids":[["0.380218","10.565"]],
  "asks":[["0.381930","1285.371"]]
}
```

`GET /api/orderbook/<PAIR>?depth=<DEPTH>`

### Parameters

Parameter | Description
--------- | -----------
PAIR | string e.g. HIVE_HBD
DEPTH | int number 1-1000


## Get Assets

> Example:

```shell
curl "https://api.hivedex.io/api/assets"
```

> Response:

```json
{
  "HIVE": {
    "name": "hive",
    "unified_cryptoasset_id": 5370,
    "circulating_supply": "342001138.102",
    "total_supply": "397064614.148",
    "max_supply": "no limit"
  },
  "HBD": {
    "name": "hive backed dollar",
    "unified_cryptoasset_id": 5375,
    "circulating_supply": "8653800.273",
    "total_supply": "28458148.845",
    "max_supply": "30% of HIVE market cap"
  }
}
```


`GET /api/assets`

Get all the assets and their information.


## Get Trades

> Example:

```shell
curl "https://api.hivedex.io/api/trades/HIVE_HBD?limit=1"
```

> Response:

```json
[
    {
        "trade_id": 3438064,
        "price": "0.383221",
        "base_volume": "6.401",
        "quote_volume": "2.453",
        "type": "buy",
        "timestamp": 1674321684000
    }
]
```


`GET /api/trades/<PAIR>?limit=<LIMIT>`

Get trade history of a pair. The data is for the last 24 hours.

### Parameters

Parameter | Description
--------- | -----------
PAIR | string e.g. HIVE_HBD
LIMIT | int number - use 0 for all the data


## Get Tickers

> Example:

```shell
curl "https://api.hivedex.io/api/tickers"
```

> Response:

```json
[
  {
    "ticker_id": "HIVE_HBD",
    "base_currency": "HIVE",
    "quote_currency": "HBD",
    "base_id": 5370,
    "quote_id": 5375,
    "last_price": "0.383184",
    "base_volume": "207144.873",
    "quote_volume": "78305.525",
    "bid": "0.382976",
    "ask": "0.383180",
    "high": "0.385845",
    "low": "0.361920"
  }
]
```


`GET /api/tickers`

Get trading information of all the pairs.

