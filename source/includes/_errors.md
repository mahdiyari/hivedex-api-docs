# Errors

> Example:

```shell
curl "https://api.hivedex.io/api/trades/test"
```

> Response:

```json
{
    "error": "ticker_id is not valid."
}
```

The following error code is returned for invalid parameters.

Error Code | Meaning
---------- | -------
400 | Bad Request -- Your request is invalid.
